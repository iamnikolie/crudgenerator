<?php

namespace Iamnikolie\Crudgenerator\Commands;

use File;
use Illuminate\Console\Command;

class CrudCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:generate
                            {structure : Fields name for the form & model.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Crud including controller, model, views & migrations.';

    /** @var string  */
    protected $routeName = '';

    /** @var string  */
    protected $controller = '';

    /**
     * Create a new command instance
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fillableArray = [];

        $file = $this->argument('structure');
        $structureContent = File::get(base_path() . '/' .$file);
        $decodedStructureContent = json_decode($structureContent);
        foreach ($decodedStructureContent->modules as $item){
            $tmp = explode('->', $item->name);
            if (sizeof($tmp) > 1 ) {
                $tmp = explode('->', $item->name);
                $name = $tmp[0] . ' to ' . $tmp[1];
                $modelName = str_singular(str_replace(' ', '', ucwords($name)));
                $migrationName = str_plural(snake_case($name    ));
            } else {
                $name = $item->name;
                $modelName = str_singular(ucfirst($item->name));
                $migrationName = str_plural(snake_case($item->name));
            }

            $tableName = $migrationName;

            if (isset($item->pk)) {
                $primaryKey = $item->pk;
            } else {
                $primaryKey = 'id';
            }

            $routeGroup = $decodedStructureContent->route_group;
            $this->routeName = ($routeGroup) ? $routeGroup . '/' . snake_case($name, '-') : snake_case($name, '-');

            $controllerNamespace = ($routeGroup) ? $routeGroup . '\\' : '';
            $fillableArray = [];
            $fillable = '';
            for ($i = 0; $i < count($item->attributes); $i++) {
                $attribute = explode(":", trim($item->attributes[$i]));
                array_push($fillableArray, '\'' . $attribute[0] . '\'');
            }
            $fillable = '[' . implode($fillableArray, ',') . ']';

            $this->call('crud:migration', [
                'name' => $migrationName,
                '--structure' => $this->argument('structure'),
                '--module' => $item->name
            ]);
            $this->call('crud:model', [
                'name' => $modelName,
                '--fillable' => $fillable,
                '--table' => $tableName,
                '--pk' => $primaryKey
            ]);
            $this->call('crud:controller', [
                'name' => $controllerNamespace . str_plural($modelName) . 'Controller',
                '--model-name' => $modelName,
                '--route-group' => $routeGroup
            ]);

            $routeFile = app_path('Http/routes.php');
            if (file_exists($routeFile)) {
                $this->controller = ($controllerNamespace != '')
                    ? $controllerNamespace . str_plural($modelName) . 'Controller'
                    : str_plural($modelName) . 'Controller';

                $isAdded = File::append($routeFile, "\n" . implode("\n", $this->addRoutes()));

                if ($isAdded) {
                    $this->info('Crud/Resource route added to ' . $routeFile);
                } else {
                    $this->info('Unable to add the route to ' . $routeFile);
                }
            }

        }

        $this->callSilent('optimize');

    }

    /**
     * Add routes.
     *
     * @return  array
     */
    protected function addRoutes()
    {
        return ["Route::resource('" . str_plural(strtolower($this->routeName)) . "', '" . $this->controller . "');"];
    }
}
